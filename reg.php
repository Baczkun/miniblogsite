<?php
session_start();
include "db_conn.php";

if(isset($_POST['uname']) && isset($_POST['email']) &&isset($_POST['pword']) && isset($_POST['cpword'])){

	function validate($data){
		$data = trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	$uname = validate($_POST['uname']);
	$email = validate($_POST['email']);
	$pass = validate($_POST['pword']);
	$cpass = validate($_POST['cpword']);

	$user_data = 'uname='. $uname. '&email='. $email;


	if (empty($uname)) {
		header("Location: register.php?error=Username is required&$user_data");
		exit();
	}
	else if (empty($email)) {
		header("Location: register.php?error=Email is required&$user_data");
		exit();
	}
	else if (empty($pass)) {
		header("Location: register.php?error=Password is required&$user_data");
		exit();
	}
	else if (empty($cpass)) {
		header("Location: register.php?error=Password is required&$user_data");
		exit();
	}
	else if($pass !== $cpass){
        header("Location: register.php?error=Password does not match&$user_data");
	    exit();
	}

	else{
		$pass = md5($pass);
		$sql = "SELECT * FROM users WHERE username = '$uname'";
		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) > 0) {
			header("Location: register.php?error=Username already taken&$user_data");
	    exit();
		}else{
			$sql2 = "INSERT INTO users(username, password, email) VALUES('$uname', '$pass', '$email')";
			$result2 = mysqli_query($conn, $sql2);
			if ($result2) {
				header("Location: register.php?success=Your Account has been Created&$user_data");
			}else{
				header("Location: register.php?error=Unknown error&$user_data");
	    		exit();

			}
		}
	}
}else{
	header("Location: register.php");
	exit();
}