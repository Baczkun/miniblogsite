<?php
session_start();

if(isset($_SESSION['id']) && isset($_SESSION['email'])){

?>
<!DOCTYPE html>
<html>
<heead>
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Home</title>
</heead>
<body>
	<nav>
		<label class="MiniBlog">MiniBlog</label>
		<ul>
			<li>Hi! <?php echo $_SESSION['username']; ?></li>
			<li><a href="home.php">Home</a></li>
			<li><a href= "logout.php">Logout</a></li>
		</ul>
	</nav>
	
	<form action="createpost-process.php" method="post">
		<p id="cpost">Create a Post!</p>
		<?php if (isset($_GET['error'])) { ?>
			<p class="error"><?php echo $_GET['error'];?></p>
		<?php } ?>
		<?php if (isset($_GET['success'])) { ?>
			<p class="success"><?php echo $_GET['success'];?></p>
		<?php } ?>
		<input type="text" id="delete" name="title" placeholder="Enter Title">
		<input type="text" id="edit" name="content" placeholder="Enter Content">
		<button type="submit" id ="post">POST</button>
	</form>
	
</body>
</html>

<?php
}else{
	header("Location: index.php");
	exit();
}
?>