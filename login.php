<?php
session_start();
include "db_conn.php";

if(isset($_POST['email']) && isset($_POST['pword'])){

	function validate($data){
		$data = trim($data);
		$data = stripcslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	$email = validate($_POST['email']);
	$pass = validate($_POST['pword']);

	if (empty($email)) {
		header("Location: index.php?error=email is required");
		exit();
	}else if (empty($pass)) {
		header("Location: index.php?error=password is required");
		exit();
	}else{
		$pass = md5($pass);
		$sql = "SELECT * FROM users WHERE email = '$email' AND password = '$pass'";

		$result = mysqli_query($conn, $sql);
		if (mysqli_num_rows($result) === 1) {
			$row = mysqli_fetch_assoc($result);
			if ($row['email'] === $email && $row['password'] === $pass) {
				$_SESSION['id'] = $row['id'];
				$_SESSION['username'] = $row['username'];
				$_SESSION['email'] = $row['email'];
				header("Location: home.php");
				exit();
			}else{
				header("Location: index.php?error=Incorrect email or password");
				exit();
			}
		}else{
			header("Location: index.php?error=Incorrect email or password");
			exit();
		}

	}
}else{
	header("Location: index.php");
	exit();
}