<?php
session_start();
include "db_conn.php";

if(isset($_POST['title']) && isset($_POST['content'])){
		function validate($data){
		$data = trim($data);
		return $data;
}
	$title = validate($_POST['title']);
	$content = validate($_POST['content']);

if (empty($title)) {
		header("Location: editpost.php?error=Can't be Empty");
		exit();
	}
	else if (empty($content)) {
		header("Location: editpost.php?error=Can't be Empty");
		exit();
	}
	else{
		$sql = "INSERT INTO contents(title, content) VALUES('$title', '$content')";
		$result = mysqli_query($conn, $sql);
		if ($result) {
				header("Location: editpost.php?success=Your Post has been Edited");
			}else{
				header("Location: editpost.php?error=Unknown error");
	    		exit();

			}
	}

}else{
	header("Location: editpost.php");
	exit();
}