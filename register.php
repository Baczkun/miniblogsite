<!DOCTYPE html>
<html>
<heead>
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>Register</title>
</heead>
<body>
	<nav>
		<label class="MiniBlog">MiniBlog</label>
		<ul>
			<li><a href="index.php">Login</a></li>
		</ul>
	</nav>
	<form action="reg.php" method="post">
		<h2>See the Registration Rules</h2>
		<?php if (isset($_GET['error'])) { ?>
			<p class="error"><?php echo $_GET['error'];?></p>
		<?php } ?>

		<?php if (isset($_GET['success'])) { ?>
			<p class="success"><?php echo $_GET['success'];?></p>
		<?php } ?>
		<input type="text" name="uname" placeholder="Enter Username"><br>
		<input type="text" name="email" placeholder="Enter Email"><br>
		<input type="text" name="pword" placeholder="Enter Password"><br>
		<input type="text" name="cpword" placeholder="Confirm Password"><br>
		<button type="submit">REGISTER</button><br><br><br>
		<p>Return to the <a id = "return" href="index.php">LOGIN PAGE</a></p>
	</form>
</body>
</html>